

**--- Hitta.se MapBox ---**

A basic MapBox-based application, built in React.js

-- **Pre-requisites** --

-   NodeJS

-- **Install** --

-   Run 'npm install' in directory from command prompt or development environment terminal

-- **How to use** --

-   Run 'npm start' in directory from command prompt or development environment terminal.

-   The map will be centered in Stockholm, above Hitta.se Main Office along with a default marker.

-   To add new marker; Simply click anywhere on map and a popup-window will appear above position with input field. 
    Enter name for marker and click 'Set Marker' to save your marker.
    
-   Click any existing marker to view its name and center map on it.

-   On top left of the Map view is a list of all markers listed in order of creation. Clicking any entry will bring
    you to its respective marker, centering map view and zooming in on it. To the right of each entry is a checkbox
    for purposes of viewing visited markers and a button to delete each marker.
    
    
    
-- **Developer's Notes** --

-   This application makes use of a third party node module to implement MapBox into React.js-projects; 
    React-mapbox-gl by users alex3165 and philpl. Though basic procedure in implementation is simplified via this 
    module, it limits certain aspects; such as element properties and custom attributes.
-   Due to these difficulties, a number of shortcuts had to be made in order to fulfill the application's key 
    functions.
    
-   There are a number of other third party modules to implement MapBox and so it's advisable to investigate further
    before choosing to work with React-mapbox-gl.
    
-   Majority of the code is centered in one single component. If further was to be applied on the application, the
    code would've been divided and re-organized into multiple components and methods would've been re-considered and
    optimized.
    
