import React, {Component} from 'react';

// Redux Store
import store from "./Store";

import logo from './logo.svg';
import './App.css';

// Mapbox Component
import {MapView} from "./MapView/MapView";

// Redux Store Provider
import {Provider} from "react-redux";



class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <MapView/>
                </div>
            </Provider>
        );
    }
}

export default App;
