import React, {Component} from 'react';
import ReactMapboxGl, {Layer, Popup, Feature} from "react-mapbox-gl";

import './MapView.css';
import store from "../Store";



let react_mapbox_gl = require("react-mapbox-gl");                   // Declaring Mapbox Settings
let layer = ReactMapboxGl.Layer;
let feature = ReactMapboxGl.Feature;

const Map = ReactMapboxGl({
    container: 'Map',
    accessToken: "pk.eyJ1IjoiYXJ2aWR1cyIsImEiOiJjamFyd3I4MWQ1NWZkMzNwN3BqeG1hNGo0In0.Wtk27hioiHrm2arH3L6uUw",
    classes: ["name"]
});


export class MapView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultZoom: [11],                                      // Default map zoom value
            centerCoord: [18.054856299999983, 59.3398016],          // Centered Coordinates (Hitta.se HQ default)
            markers: [{                                             // Marker object state
                input: "Hitta.se HQ",                               // Initial Test Marker
                coordinates: [18.054856299999983, 59.3398016],
                id: "layer-1"
            }],
            nameTemp: "",                                           // State for Popup input
            mapClicked: false,                                      // State Bool to keep Popup open
            filterInput: "",                                        // State for list filter input

            markerClicked: {                                        // State object for clicked markers
                open: false,
                input: "",
                coordinates: []
            }
        };
        store.dispatch({
            type: "POP", payload: {                          // Store state for saving coordinates when creating marker
                coordinates: []
            }
        });
        this.closeButton = this.closeButton.bind(this);
    }

    getCoordinates = (map, evt) => {                                // Method for getting coordinates on map-click

        // console.log(this.state.markers);

        let coordinates = [evt.lngLat.lng, evt.lngLat.lat];

        if (this.state.markerClicked.open) {
            this.setState({
                markerClicked: {
                    open: false
                }
            })
        }
        store.dispatch({
            type: "POP", payload: {
                open: true,
                coordinates: coordinates
            }
        });
        this.setState({
            mapClicked: true
        });
    };

    markerClick = ({feature}) => {                                  // Method for getting marker-data on click

        this.setState({
            mapClicked: false
        });

        let coordinates = feature.geometry.coordinates;
        let tempObject = this.state.markers;
        let name = "";

        for (let i = 0; i < tempObject.length; i++) {
            if (tempObject[i].id === feature.layer.id) {
                name = tempObject[i].input;
                break;
            }
        }

        this.setState({
            centerCoord: coordinates,
            markerClicked: {
                open: true,
                input: name,
                coordinates: coordinates
            }
        })
    };

    saveMarker() {                                                  // Method for saving new marker

        let newMarker = {
            input: this.state.nameTemp,
            coordinates: store.getState().coordinates,
            id: "layer-" + (this.state.markers.length + 1)
        };

        this.setState({
            markers: this.state.markers.concat(newMarker),
            nameTemp: "",
            mapClicked: false
        });

    }

    deleteMarker(e){                                                // Method for deleting marker
        let tempObject = this.state.markers;
        let tempId  = e.target.id;

        for (let i = 0; i < tempObject.length; i++) {
            if (e.target.id === ("delete-" + tempObject[i].id)) {
                this.setState({
                    markers: this.state.markers.filter((_, j) => j !== i)
                });
                break;
            }
        }

    }

    closeButton() {                                                 // Method for closing Popup
        this.setState({
            mapClicked: false,
            nameTemp: "",
            markerClicked: {
                open: false
            }
        });
    }

    listClick(e) {                                                 // Method for centering map on marker in list
        let data = JSON.parse(e.target.attributes.getNamedItem('data').value);
        this.setState({
            centerCoord: data,
            defaultZoom: [15]
        })
    }

    popupUpdate(e) {                                               // Method for updating input state for popup
        this.setState({
            nameTemp: e.target.value
        });
    }

    filterUpdate(e) {                                              // Method for updating input state for list filter
        this.setState({
            filterInput: e.target.value,
        })
    }


    render() {

        const {
            filterInput,
            defaultZoom,
            centerCoord,
            markers,
            nameTemp,
            mapClicked,
            markerClicked
        } = this.state;

        // Generated filtered list \\
        const list = this.state.markers
            .filter(d => filterInput === '' || d.input.includes(filterInput))
            .map((d, i) => <li key={i}><span data={JSON.stringify(d.coordinates)}
                                          onClick={this.listClick.bind(this)}>{d.input}</span>
                                   <input id={"visited-" + d.id} data={JSON.stringify(d.coordinates)}
                                type="checkbox" name="visited" value="Car"/>
                <b className="deleteButton" id={"delete-" + d.id} onClick={this.deleteMarker.bind(this)}>X</b></li>);

        return (
            <div className="mapView">
                <Map
                    style="mapbox://styles/mapbox/streets-v9"
                    zoom={defaultZoom}
                    center={centerCoord}
                    onClick={this.getCoordinates}
                    containerStyle={{
                        height: "100vh",
                        width: "100%"
                    }}>
                    {
                        markers.map((marker, i) => {
                            return <Layer
                                key={i}
                                type="symbol"
                                layout={{"icon-image": "star-15"}}>
                                <Feature
                                    coordinates={marker.coordinates}
                                    onClick={this.markerClick.bind(this)}/>
                            </Layer>
                        })
                    }
                    {
                        mapClicked ? (<Popup
                                coordinates={store.getState().coordinates}
                                id={"popup"}>
                                <b className="xButton" onClick={this.closeButton}>X</b>
                                <label>
                                    Set name for marker: <br/>
                                    <input type="text" name="name" value={nameTemp}
                                           onChange={this.popupUpdate.bind(this)}/>
                                </label>
                                <button onClick={this.saveMarker.bind(this)}> Set Marker</button>
                            </Popup>
                        ) : null
                    }
                    {
                        markerClicked.open ? (
                            <Popup key={markerClicked.input}
                                   coordinates={markerClicked.coordinates}>
                                <b className="xButton" onClick={this.closeButton}>X</b>
                                <div className="popupContent">{markerClicked.input}</div>
                            </Popup>
                        ) : null
                    }
                </Map>
                <div className="markerListDiv">

                    <input value={filterInput} placeholder="Filter..." type="text" onChange={this.filterUpdate.bind(this)}/>
                    <ul>{list}</ul>

                </div>

            </div>


        );
    }
}